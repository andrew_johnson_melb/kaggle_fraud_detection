# Structured data analysis notes

This project is a collection of useful ideas, code, and papers for 
building and interpreting models for structured data.

### Install

```
# Clone repo
git clone https://gitlab.com/andrew_johnson_melb/kaggle_fraud_detection.git

# Install requirements
pip install -r requirements.txt
```

###  1. Class imbalance

* Systematic study on dealing with class imbalance in the context on CNN https://arxiv.org/abs/1710.05381

From the paper:

*Based on results from our experiments we conclude that (i) the effect of class imbalance   
on classification performance is detrimental; (ii) the method of addressing class imbalance   
that emerged as dominant in almost all analyzed scenarios was oversampling; (iii) oversampling   
should be applied to the level that completely eliminates the imbalance, whereas the optimal   
undersampling ratio depends on the extent of imbalance; (iv) as opposed to some classical   
machine learning models, oversampling does not cause overfitting of CNNs; (v)   
thresholding should be applied to compensate for prior class probabilities when   
overall number of properly classified cases is of interest*

Important point, only oversampling the training set.

Basic method: 

Random minority oversampling, which simply replicates randomly
selected samples from minority classes

Current tools: https://imbalanced-learn.readthedocs.io/en/stable/api.html

### 2. Time Component in the validation set

We should calibrate the validation set, to make sure it aligns with the test set
performance.

* Performance on val set is much higher when using random shuffe
* Need to figure out which validation set better correlations with the Kaggle Test set

### 3. Model Explainability

https://github.com/slundberg/shap or lime. There is a function within lightgbm that 
will return feature contribution. 

Lightgbm and Xgboost both work with https://github.com/slundberg/shap. SHAP
creates very nice plots showing the features contributions. When calling 
pred_prob in lightGBM one


### 5. Effectively tunning Hyper-parameters

https://github.com/fmfn/BayesianOptimization

For an sklearn example, see https://github.com/fmfn/BayesianOptimization

