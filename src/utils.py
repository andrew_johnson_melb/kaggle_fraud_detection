# Helper function for Kaggle Fraud comp

import pandas as pd
import numpy as np
from pandas.api.types import is_string_dtype
from pandas.api.types import is_numeric_dtype
import seaborn as sns
import matplotlib.pylab as plt


def create_df_subsample(df_fraud, subsample_frac):
    """
    Create a subsample of the main dataset, but keep 
    all fraud detections. Expects 'isFraud' column
    
    This sampling will preserve the time-ordered nature of 
    the columns.
    """
    # TODO add weights to sampling
    print(f'Frac subsampling = {subsample_frac}')
    df = df_fraud.copy()
    fraud_bool = df.isFraud == 1
    frad_rows = df[fraud_bool]
    non_fraud_rows = df[~fraud_bool].sample(frac=subsample_frac)
    df = pd.concat([frad_rows, non_fraud_rows], axis=0)
    df = df.sort_values(by='TransactionDT')
    #df = randomly_shuffle_df(df)
    df.reset_index(drop=True, inplace=True)
    return df

def check_col_overlap(df1, df2, ignore_dep):
    # Check the col name overlap between two dataframes
    # The order will be important here.
    cols_missing = list(set(df1.columns.values) - set(df2.columns.values))
    cols_missing.remove(ignore_dep)
    return cols_missing


def check_train_test_consistency(df_train, df_test, dependent_var='isFraud'):
    """Make sure the train and test sets have the same shape.The columns present 
    can be different when one-hot encoding features as some classes may be missing.    
    """
    print(f'Size train feature set = {df_train.shape[1]}')
    print(f'Size test feature set = {df_test.shape[1]}')
    cols_missing_from_test = check_col_overlap(df_train, df_test, ignore_dep=dependent_var)
    print(f'Missing features in test set {cols_missing_from_test}')
    for feature_name in cols_missing_from_test:
        print(f'Adding feature = {feature_name} to test set..')
        df_test[feature_name] = 0
    # The training data still has the dependent variable.
    assert df_train.shape[1] - df_test.shape[1] == 1, "Shape Mismatch: Training and test sets have different sizes"
    return df_train, df_test

def compute_feature_missing_pct(df):
    # Find how much data is missing
    return (100 * df.isnull().sum()/len(df)).sort_values(ascending=False)