import seaborn as sns
import matplotlib.pylab as plt
import pandas as pd

sns.set_style("whitegrid")


def rf_feat_importance(m, df):
    return pd.DataFrame({'cols':df.columns.values, 'imp':m.feature_importances_}
                       ).sort_values('imp', ascending=False)

def plot_fi(fi): 
    return fi.plot('cols', 'imp', kind='barh', figsize=(12,7), legend=False)


def plot_importance(model, X , num = 30, save_dir='./plots/'):
    feature_imp = pd.DataFrame(sorted(zip(model.feature_importances_,X.columns)), 
                               columns=['Value','Feature'])
    plt.figure(figsize=(12, 6))
    sns.set(font_scale = 1.1)
    sns.set_style("whitegrid")
    sns.barplot(x="Value", y="Feature", data=feature_imp.sort_values(by="Value", 
                                                        ascending=False)[0:num], palette="Blues_d")
    plt.title('LightGBM Feature plot_importance')
    plt.tight_layout()
    plt.show()
    plt.savefig(f'{save_dir}/lgbm_importances-01.png')


def plot_model_complexity_curve(m, metric='auc'):
    """Plot boosting rounds vs Model Performance
    for both the train and test set"""
    train_metric = m.evals_result_['Train'][metric]
    validation_metric = m.evals_result_['Validation'][metric]
    boosting_rounds = list(range(len(train_metric)))
    results = pd.DataFrame(
        {'Train set' : train_metric, 
        'Validation set' : validation_metric}, 
        index=boosting_rounds)
    ax=results.plot(figsize=(10,4))
    ax.set_xlabel('Boosting Rounds')
    ax.set_ylabel('AUC metric')
    plt.legend();


def create_scatter(df):
	# Relationship between fraud and datetime and fraud and TranscationID
	ax = plt.figure(figsize=(13,6))
	plt.title('Transaction ID/DT vs Is Fraud')
	sns.scatterplot(x='TransactionID', y='TransactionAmt', hue='isFraud', data=df.sample(10000))


def plot_cat_feature(df, feature_name='ProductCD', hue_feature='isFraud'):
	# Show feature distribtion
	plt.figure(figsize=(12,4))
	plt.suptitle('ProductCD Distributions', fontsize=15)
	plt.subplot(121)
	sns.countplot(x=feature_name, data=df)
	plt.subplot(122)
	sns.countplot(x=feature_name, hue=hue_feature, data=df)

