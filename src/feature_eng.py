import numpy as np

def add_features(df, feature_set_names=None):
    # Feature eng functions.
    
	if 'dt' in feature_set_names:
		print('Adding new datatime indicators..')
		df['tras_day_of_week'] = np.floor((df['TransactionDT'] / (3600 * 24) - 1) % 7)
		df['tras_hour_of_week'] = np.floor((df['TransactionDT'] / 3600) % 24)
    
	if 'agg_spend_stats' in feature_set_names:
		print('Adding aggreate amount spent stats...')
		# Add aggregrate features -- 
		df['TransactionAmt_to_mean_card1'] = df['TransactionAmt'] / df.groupby(['card1'])['TransactionAmt'].transform('mean')
		df['TransactionAmt_to_mean_card4'] = df['TransactionAmt'] / df.groupby(['card4'])['TransactionAmt'].transform('mean')
		df['TransactionAmt_to_std_card1'] = df['TransactionAmt'] / df.groupby(['card1'])['TransactionAmt'].transform('std')
		df['TransactionAmt_to_std_card4'] = df['TransactionAmt'] / df.groupby(['card4'])['TransactionAmt'].transform('std')

		df['id_02_to_mean_card1'] = df['id_02'] / df.groupby(['card1'])['id_02'].transform('mean')
		df['id_02_to_mean_card4'] = df['id_02'] / df.groupby(['card4'])['id_02'].transform('mean')
		df['id_02_to_std_card1'] = df['id_02'] / df.groupby(['card1'])['id_02'].transform('std')
		df['id_02_to_std_card4'] = df['id_02'] / df.groupby(['card4'])['id_02'].transform('std')

		df['D15_to_mean_card1'] = df['D15'] / df.groupby(['card1'])['D15'].transform('mean')
		df['D15_to_mean_card4'] = df['D15'] / df.groupby(['card4'])['D15'].transform('mean')
		df['D15_to_std_card1'] = df['D15'] / df.groupby(['card1'])['D15'].transform('std')
		df['D15_to_std_card4'] = df['D15'] / df.groupby(['card4'])['D15'].transform('std')

		df['D15_to_mean_addr1'] = df['D15'] / df.groupby(['addr1'])['D15'].transform('mean')
		df['D15_to_mean_card4'] = df['D15'] / df.groupby(['card4'])['D15'].transform('mean')
		df['D15_to_std_addr1'] = df['D15'] / df.groupby(['addr1'])['D15'].transform('std')
		df['D15_to_std_card4'] = df['D15'] / df.groupby(['card4'])['D15'].transform('std')

	if 'email' in feature_set_names:
		df['P_email']=(df['P_emaildomain']=='xmail.com')
		df['R_email']=(df['R_emaildomain']=='xmail.com')
		df['P_email']=(df['P_emaildomain']=='xmail.com')
		df['R_email']=(df['R_emaildomain']=='xmail.com')

	return df