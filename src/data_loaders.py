"""
Utils for helping loading raw data
"""

import os
import pandas as pd
from src.utils import create_df_subsample

def create_test_set(load_path='./data/', save_dir='./tmp', verbose=True):
    """Load in all the test data, merge dataframes, and then save as a feather file.."""
    print(f'Loading test data from location = {load_path}')
    df_test_transaction = pd.read_csv(f'{load_path}test_transaction.csv', low_memory=False)
    df_test_identity = pd.read_csv(f'{load_path}test_identity.csv', low_memory=False)
    X_test_set_raw = df_test_transaction.merge(right=df_test_identity, how='left', on='TransactionID')
    if verbose:
        feature_count = df_test_transaction.shape[1]
        row_count = df_test_transaction.shape[0]
        feature_count = df_test_identity.shape[1]
        row_count = df_test_identity.shape[0]
        print(f'Number of features = {feature_count} (transaction only)')
        print(f'Number of rows = {row_count} (transaction only)')
        print(f'Number of features = {feature_count} (identity only)')
        print(f'Number of rows = {row_count} (identity only)')
    os.makedirs(save_dir, exist_ok=True)
    path = f'{save_dir}/test_set_raw.feather'
    print(f'Create file {path}')
    X_test_set_raw.to_feather(path)


def create_train_set(load_path='./data/', save_dir='./tmp', subsample=0.3, verbose=True):
    """
    Create a binary file for the training using a subsample of the data
    """
    df_transaction_raw = pd.read_csv(f'{load_path}train_transaction.csv', low_memory=False)
    df_identity_raw = pd.read_csv(f'{load_path}train_identity.csv', low_memory=False)
    if verbose:
        feature_count = df_transaction_raw.shape[1]
        row_count = df_transaction_raw.shape[0]
        feature_count = df_identity_raw.shape[1]
        row_count = df_identity_raw.shape[0]
        print(f'Number of features = {feature_count} (transaction only)')
        print(f'Number of rows = {row_count} (transaction only)')
        print(f'Number of features = {feature_count} (identity only)')
        print(f'Number of rows = {row_count} (identity only)')
    # Combine the transaction and identity datasets.
    df_raw = df_transaction_raw.merge(right=df_identity_raw, how='left', on='TransactionID')
    print('Creating subsampe of main dataset...')
    if subsample is not None:
    	df_raw = create_df_subsample(df_raw, subsample_frac=0.3)
    os.makedirs(save_dir, exist_ok=True)
    path = f'{save_dir}/train_raw_sample.feather'
    print(f'Creating file {path}')
    df_raw.to_feather(path)