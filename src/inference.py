
from src.training_utils import create_final_train_set
from src.training_utils import random_over_sampler

from sklearn.model_selection import KFold
import pandas as pd
import numpy as np
from lightgbm import LGBMClassifier

def predict_test_set(m, X, file_prefix, df_test_raw):
    """Make forcast on the test set using the model m
    df_raw is needed as it contains the TransactionID values.
    """
    save_dir = f'./predictions/fraud_pred_{file_prefix}.csv'
    print(f'Saving data to {save_dir}')
    pos_prediction = m.predict_proba(X)[:,1]
    prediction_test_set = pd.Series(pos_prediction, index=df_test_raw.TransactionID.values)
    prediction_test_set.name = 'isFraud'
    prediction_test_set.index.name = 'TransactionID'
    prediction_test_set.to_csv(save_dir, header=['isFraud'])
    

def save_prediction(pos_prediction, file_prefix, df_test_raw):
    """Make forcast on the test set using the model m
    df_raw is needed as it contains the TransactionID values.
    """
    save_dir = f'./predictions/fraud_pred_{file_prefix}.csv'
    print(f'Saving data to {save_dir}')
    prediction_test_set = pd.Series(pos_prediction, index=df_test_raw.TransactionID.values)
    prediction_test_set.name = 'isFraud'
    prediction_test_set.index.name = 'TransactionID'
    prediction_test_set.to_csv(save_dir, header=['isFraud'])


def pred_with_cross_val(df_train, df_test, features_to_drop, model_params, 
                        oversample=True, n_folds=8, seed=42):
    """
    Make predictions for the provided test set 'df_test' using the average of the 
    predictions from cross-validation.
    """
    # Create X, y using the full dataset
    X, y = create_final_train_set(df_train, features_to_drop)

    # resample training data.
    if oversample:
        print("Oversampling low count class...")
        (X, y) = random_over_sampler(X, y)
    
    # Make sure the test set looks like the training set
    X_test = df_test.loc[:,X.columns.values]

    folds = KFold(n_splits=n_folds, shuffle=True, random_state=seed)
    # Create zero predictions dataframe
    predictions = np.zeros(len(X_test))
    for fold_, (trn_idx, val_idx) in enumerate(folds.split(X, y)):
        print(f'Running fold={fold_}')  
        # Create data for fold 'fold_'
        x_train_fold, y_train_fold = X.iloc[trn_idx,:], y[trn_idx]
        x_val_fold, y_val_fold = X.iloc[val_idx,:], y[val_idx]
        
        m_fold = LGBMClassifier(n_estimators=500, **model_params)
        m_fold.fit(
            X=x_train_fold.values, y=y_train_fold.values, 
            eval_names = ['Train', 'Validation'],
            eval_set=[(x_train_fold.values, y_train_fold.values),
                      (x_val_fold.values, y_val_fold.values)],
            eval_metric='auc', 
            early_stopping_rounds=40,
            verbose=40)
        # Get and average test set predictions
        prediction_fold = m_fold.predict_proba(X_test)[:,1]
        predictions += prediction_fold/n_folds
    return predictions
