
from pandas.api.types import is_string_dtype
from pandas.api.types import is_numeric_dtype
from sklearn.preprocessing import LabelEncoder
import numpy as np
import pandas as pd

def find_cat_columns(df, known_cats=[None]):
    """Construct a list of all the cat columns within the dataframe df.
    This should be run only for the training set.
    """
    list_of_cat_columns = []
    for col_name in df.columns.values:
        if is_string_dtype(df[col_name].dtype) or col_name in known_cats:
            list_of_cat_columns.append(col_name)
    return list_of_cat_columns


def encode_cat_features(df_train, df_test, max_n_cats, known_cat_variables):
    """Convert all the categorical variables into numeric values 
    by ordinal encoding.
    
    Cats with less than 'max_n_cats' classes will be one-hot 
    encoded. And NaN values will be treated as a seperate cat.
    
    #TODO, force particular cols to be one-hot encoded. Like email, 
    could be quite useful.
    """
    df_train, df_test = df_train.copy(), df_test.copy()
    cat_columns = find_cat_columns(
        df_train, known_cats=known_cat_variables)
    label_transform_dict = {}    
    for col_name in cat_columns:
        # This will convert nan's into strings and therefore they
        # will be treated as a seperate category.
        train_col = df_train[col_name].astype('str')
        test_col = df_test[col_name].astype('str')
        le = LabelEncoder()
        combined_data = np.concatenate([train_col, test_col])
        le.fit(combined_data)
        print(f'feature={col_name}: Class Count = {len(le.classes_)}')
        if len(le.classes_) > max_n_cats:
            df_train[col_name] =  le.transform(train_col)
            df_test[col_name] =  le.transform(test_col)
            label_transform_dict[col_name] = list(
                zip(le.classes_, range(len(le.classes_))))
        else:
            # Change the type to categorical. Since pandas 
            # get_dummies function will pick up these features 
            print(f'Feature = {col_name} will be one-hot encoded')
            df_train[col_name] = df_train[col_name].astype('category')
            df_test[col_name] = df_test[col_name].astype('category')
    # One-hot encode the cat variables that were not encoded.        
    df_train = pd.get_dummies(df_train)
    df_test = pd.get_dummies(df_test)
    return (df_train, df_test, label_transform_dict)


def fix_na_columns(df, col_name, add_na_bool):
    # Basic imputation which keep track of missing values with bool columns
    # using medium
    col_series = df[col_name]
    # make sure the column has a numeric datatype
    if is_numeric_dtype(col_series):
        # Make sure there are missing values.
        if pd.isnull(col_series).sum():
             # add na bool columns
            if add_na_bool:
                df[f'na_{col_name}'] = pd.isnull(col_series)
            # Update missing values with medium
            col_series_filled = col_series.fillna(value=col_series.median())
            df[col_name] = col_series_filled
    return df
    

def fix_na_dataframe(df, add_na_bool=True):
    # iterate over each column in df and fix na's
    for col_name in df.columns.values:
        df = fix_na_columns(df, col_name, add_na_bool)
    return df


def check_df_for_na(df, cols_to_check):
    # Raise exception if there are na values in the dataframe
    count_nans = df.loc[:, cols_to_check].isnull().sum(axis=0).sum()
    assert count_nans == 0, "There are nan values present in the dataframe"

def clean_inf_nan(df):
    # Convert all infinite values to np.nan.
    # by https://www.kaggle.com/dimartinot
    return df.replace([np.inf, -np.inf], np.nan)

def compute_feature_missing_pct(df):
    # Find the percentage of missing data for each feature col
    return (100 * df.isnull().sum()/len(df)).sort_values(ascending=False)
