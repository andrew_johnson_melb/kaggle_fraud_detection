# Help build training and test data.

import numpy as np
import pandas as pd
from imblearn.over_sampling import RandomOverSampler
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score 

def randomly_shuffle_df(df):
    # Randomly shuffle dataframe
    idxs = np.random.permutation(range(len(df)))
    return df.iloc[idxs,:]


def train_val_split(df, train_size, time_delta_col, split_by_time):
    # Non-random train-val split. Make sure df is ordered by time
    print(f'Ordering train/val split by {time_delta_col}')
    if split_by_time:
        df = df.sort_values(by=time_delta_col)
    else:
        df = randomly_shuffle_df(df)
    return (df.iloc[:train_size], df.iloc[train_size:])


def seperate_dependent_var(df, dependent_var):
    # Split the data into X (independent) and y (dependent) variables.
    y = df[dependent_var]
    X = df.drop(dependent_var, axis=1)
    return X,y


def create_training_val_set(df, 
                            frac_training=0.7, 
                            dependent_var='isFraud', 
                            time_delta_col='TransactionDT',
                            split_by_time=True,
                            drop_features=None, 
                            verbose=True):
    """
    Split the input dataset into a training and validation set, using 'frac_training'
    Expects the supplied 'df' to be processed.
    """
    df_proc = df.copy()
    train_size = int(len(df_proc) * frac_training)
    val_size = len(df_proc) - train_size
    df_train, df_val = train_val_split(df_proc, train_size, time_delta_col, split_by_time)
    if drop_features is not None:
        print(f'Removing features {drop_features}')
        df_train = df_train.drop(drop_features, axis=1)
        df_val = df_val.drop(drop_features, axis=1)
    X_train, y_train = seperate_dependent_var(df_train, dependent_var)
    X_val, y_val = seperate_dependent_var(df_val, dependent_var)
    if verbose:
        print(f'Using {100 * frac_training} % for Training set')
        print(f'Train size = {train_size}')
        print(f'Validation size = {val_size}')
    return (X_train, y_train, X_val, y_val)


def random_over_sampler(X_train, y_train, random_state=42):
    '''
    Wrapper around imblearn's RandomOverSampler. This method will 
    keep the column names and return pandas dataframes rather than 
    numpy arrays.
    
    Note, this should only be run on TRAINING_DATA
    '''
    ros = RandomOverSampler(random_state=random_state)
    X_res, y_res = ros.fit_resample(X_train, y_train)
    
    print("Check class imbalance..Should be equal post Oversampling")
    display(pd.Series(y_res).value_counts())
    
    X = pd.DataFrame(X_res, columns=X_train.columns.values)
    y = pd.Series(y_res, name=y_train.name)
    return X,y


def show_validation_error(model, X_train, y_train, X_val, y_val, model_name='Default', show_report=False):
    """
    Print classification report and AUC ROC metric for validation and training set.
    
    Note, ROC expects the probability score of the positive class 
    """
    y_prob_pos_val = model.predict_proba(X_val.values)[:,1]
    roc_auc_metric_val = round(roc_auc_score(y_true=y_val, y_score=y_prob_pos_val), 4)
    y_prob_pos_train = model.predict_proba(X_train.values)[:,1]
    roc_auc_metric_train = round(roc_auc_score(y_true=y_train, y_score=y_prob_pos_train), 4)
    print(f'Results for model = {model_name}:')
    print(f'\t Validation set: AUC ROC = {roc_auc_metric_val}')
    print(f'\t Training set: AUC ROC = {roc_auc_metric_train}')
    if show_report:
        y_val_estimate = model.predict(X_val.values)
        print('Classification report Validation set:')
        print(classification_report(y_val, y_pred=y_val_estimate))


def create_final_train_set(df, features_to_drop, dependent_var='isFraud'):
    """Create X, y from using the full dataset. Drop unwanted columns.
    """
    df_local = df.drop(features_to_drop, axis=1)
    X_all, y_all = seperate_dependent_var(df_local, dependent_var)
    return X_all, y_all

